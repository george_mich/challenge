# Backend Technical Assessment #

## Problem - Video Rental Store ##
Create a system to managing the rental administration

* Have an inventory of films
* Calculate the prices for rentals and returns

The price of rentals is based on the type of film rented and how many days the film is rented for. When renting,
the customer says for how many days they want to rent for and pay up front. If the film is returned late, then 
rent for the extra days are charged when returning.

The store has three type of films:

* New releases - For each day rented, the `<premium price>` is charged
* Regular films - A flat price of `<basic price>` is charged for the 3 days, and then for each additional day
`<basic price>` is charged per day
* Old film - A flat price of `<basic price>` is charged for the first 5 days, and then for each additional day,
`<basic price>` is charged per day

`<premium price>` is 40 SEK

`<basic price>` is 30 SEK

#### The API should expose operations for ####
* Renting one or several films and calculating the price
* Returning films and calculating possible surcharges

#### Examples of price calculations: ####
* Matrix 11 (New release) 1 days - 40 SEK
* Spider Man (Regular) 5 days - 90 SEK
* Spider Man 2 (Regular) 2 days - 30 SEK
* Out of Africa (Old) 7 days - 90 SEK

Total price: 250 SEK

When returning films late:

* Matrix 11 (New release) 2 extra days - 80 SEK
* Spider Man (Regular) 1 days - 30 SEK

Total late charge: 110 SEK

## Approaches ##

The project is created via [Spring Initializr](https://start.spring.io/) mainly for four reasons:

* It creates a standardized project structure
* It enables you with few clicks to generate a project with the chosen technologies and dependencies
* Adds the spring boot maven plugin which packaging the project
* Speeds up the process

### Technologies, Tools and Libraries  Used ###
* Maven
* Java 11
* Spring Boot 2.5.5
* [Spring Data](https://spring.io/projects/spring-data-jpa)
* [Spring Fox](http://springfox.github.io/springfox/)
* Lombok
* H2 Database
* Jacoco Maven Plugin
* Surefire Maven Plugin
* Jib Maven Plugin
* Docker

### Focused ###
* Project Structure
* Data modeling 
* Object Oriented and SOLID Principles
* Quality
* Testing (Unit Testing and Integration Testing)
* Deployment

The project is organized based on a business domain structure, where each domain has its own relevant classes.
A typical business domain will have an Entity, Repository, Service and Controller classes where each one have 
its own responsibility while at the same time achieving separation of concerns.

Regarding the [data modelling](images/db-schema.png), the entities are kept simple with few fields and keep the necessary hierarchy.
The idea behind the modeling, its extensible which allows you to add new movies where it's enough to specify the type.
If you need to change the type, simply use update query to update the reference. If you need to change the price of any type, 
this will reflect to all movies without the need to update all of them.

Code quality and testing was having high priority during the development of this assessment.
For the scope of this exercise, **unit and integrations tests** are written using popular testing libraries like
**JUnit 5, mockito, mockMvc and SpringBootTest** in order to cover the different layers of the application. As part of this
strategy, the tests are classified as Unit Tests and Integration Tests and are running on different phases during the 
build time. It is well known that, integration tests are much slower than unit tests, thus it was decided to be run only on
**verify** maven phase with the help of the **failsafe maven plugin**. Running integration tests on different phase it speeds up
the development time, where the developer can run more frequent the unit tests during the development in order verify the 
changes, as these tests can run in few seconds. Last but not least **jacoco maven plugin** used to report the coverage 
which includes both unit and integration tests.

As part of the deployment, **jib maven plugin** is used to create a docker image of the service on the **install** maven phase.

### Not Focused ###
* API Authentication/Authorization
* Complex data models
* Rental Management and API CRUD Operations
* Liquibase (Data migration and version control)

### How to run ###
There are two possible ways to run the application and in both cases, maven is required:

* **Java JDK** | Java 11 JDK is required
  * mvn clean verify
  * java -jar ./target/challenge.jar

#### OR ####

* **Docker** | Docker engine is required
  * mvn clean install
  * docker run -p 8080:8080 challenge:0.0.1

### Database Connection Details ###
  * Web console: [Click here](http://localhost:8080/h2-console/)
  * Username: admin
  * Password: admin
  * JDBC URL: jdbc:h2:mem:db
  * Driver Class: org.h2.Driver


### Swagger API Documentation ###
[Click here](http://localhost:8080/swagger-ui/)

### Test Coverage Report ###
* mvn clean verify
* ./target/site/index.html

### SonarQube Code Analysis
[Click here](images/sonarqube-analysis.png)

### Contact Details ###
**Email:** george.michpro@outlook.com

[Linkedin](https://www.linkedin.com/in/george-michael-17440b11b)