package com.demo.challenge.common.data;

import com.demo.challenge.film.Film;
import com.demo.challenge.filmtype.FilmType;
import com.demo.challenge.filmtype.FilmTypeEnum;
import com.demo.challenge.pricetype.PriceType;
import com.demo.challenge.pricetype.PriceTypeEnum;

public class FilmBuilder {

    public Film getNewReleaseFilm(long id, String name) {

        FilmType newRelease = FilmType.builder()
                .withType(FilmTypeEnum.NEW_RELEASE.getValue())
                .withDefaultRentalDays(1)
                .withPriceType(getPremiumPrice())
                .build();

        return Film.builder()
                .withId(id)
                .withName(name)
                .withFilmType(newRelease)
                .build();
    }

    public Film getRegularFilm(long id, String name) {

        FilmType regular = FilmType.builder()
                .withType(FilmTypeEnum.REGULAR.getValue())
                .withDefaultRentalDays(3)
                .withPriceType(getBasicPrice())
                .build();

        return Film.builder()
                .withId(id)
                .withName(name)
                .withFilmType(regular)
                .build();
    }

    public Film getOldFilm(long id, String name) {

        FilmType old = FilmType.builder()
                .withType(FilmTypeEnum.OLD.getValue())
                .withDefaultRentalDays(5)
                .withPriceType(getBasicPrice())
                .build();

        return Film.builder()
                .withId(id)
                .withName(name)
                .withFilmType(old)
                .build();

    }

    private PriceType getPremiumPrice() {
        return PriceType.builder()
                .withType(PriceTypeEnum.PREMIUM.getValue())
                .withPrice(40.00)
                .build();
    }

    private PriceType getBasicPrice() {
        return PriceType.builder()
                .withType(PriceTypeEnum.BASIC.getValue())
                .withPrice(30.00)
                .build();
    }

}
