package com.demo.challenge.rental;

import com.demo.challenge.common.data.FilmBuilder;
import com.demo.challenge.film.Film;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RentalCalculatorTest {

    private final RentalCalculator rentalCalculator = new RentalCalculator();
    private final FilmBuilder filmBuilder = new FilmBuilder();

    @Test
    void calculateCostForFilmTest() {

        // given
        Film matrix11 = filmBuilder.getNewReleaseFilm(1,"Matrix 11");
        Film spiderman = filmBuilder.getRegularFilm(2,"Spider Man");
        Film spiderman2 = filmBuilder.getRegularFilm(3,"Spider Man 2");
        Film outOfAfrica = filmBuilder.getOldFilm(4, "Out of Africa");

        // when then
        assertEquals(40, rentalCalculator.calculateCostForFilm(matrix11, 1));
        assertEquals(90, rentalCalculator.calculateCostForFilm(spiderman, 5));
        assertEquals(30, rentalCalculator.calculateCostForFilm(spiderman2, 2));
        assertEquals(90, rentalCalculator.calculateCostForFilm(outOfAfrica, 7));

        assertEquals(0, rentalCalculator.calculateCostForFilm(outOfAfrica, 0));
        assertEquals(0, rentalCalculator.calculateCostForFilm(outOfAfrica, -10));

    }

    @Test
    void calculateSurchargeTest() {

        // given
        Film matrix11 = filmBuilder.getNewReleaseFilm(1,"Matrix 11");
        Film spiderman = filmBuilder.getRegularFilm(2,"Spider Man");
        Film outOfAfrica = filmBuilder.getOldFilm(3, "Out Of Africa");

        // when then
        assertEquals(80, rentalCalculator.calculateSurcharge(matrix11, 2));
        assertEquals(30, rentalCalculator.calculateSurcharge(spiderman, 1));

        assertEquals(0, rentalCalculator.calculateSurcharge(outOfAfrica, 0));
        assertEquals(0, rentalCalculator.calculateSurcharge(matrix11, -10));

    }

}