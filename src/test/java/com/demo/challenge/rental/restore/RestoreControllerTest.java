package com.demo.challenge.rental.restore;

import com.demo.challenge.common.exception.ExceptionHandlers;
import com.demo.challenge.rental.restore.dto.RestoreRentalResponse;
import com.demo.challenge.rental.restore.dto.RestoreRequest;
import com.demo.challenge.rental.restore.dto.RestoreResponse;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.demo.challenge.common.TestUtil.toJson;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(MockitoExtension.class)
class RestoreControllerTest {

    private static final String URL = "/api/rental/restore";

    @Mock
    private RestoreService restoreService;

    @InjectMocks
    private RestoreController restoreController;

    private MockMvc mockMvc;

    @BeforeEach
    void beforeEach() {
        mockMvc = MockMvcBuilders.standaloneSetup(restoreController)
                .setControllerAdvice(new ExceptionHandlers())
                .build();
    }

    @Test
    void restoreFilmsTest() throws Exception {

        // given
        RestoreRequest restoreRequest = new RestoreRequest(1L, Set.of(1L, 2L));

        List<RestoreRentalResponse> rentalResponses = List.of(
                new RestoreRentalResponse(1, 1, 10),
                new RestoreRentalResponse(2, 2, 15));

        RestoreResponse restoreResponse = new RestoreResponse(1L, rentalResponses, 25);

        when(restoreService.restoreCustomerFilms(any(RestoreRequest.class))).thenReturn(restoreResponse);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(restoreRequest)))

                // then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.customerId").value(1))

                .andExpect(jsonPath("$.rentals[0].filmId").value(1))
                .andExpect(jsonPath("$.rentals[0].rentalId").value(1))
                .andExpect(jsonPath("$.rentals[0].surcharge").value(10))

                .andExpect(jsonPath("$.rentals[1].filmId").value(2))
                .andExpect(jsonPath("$.rentals[1].rentalId").value(2))
                .andExpect(jsonPath("$.rentals[1].surcharge").value(15))

                .andExpect(jsonPath("$.totalSurcharges").value(25));
    }

    @Test
    void invalidRequestTest() throws Exception {

        // given
        String invalidRequest = "invalid request";

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(invalidRequest))
                // then
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
    }

    @ParameterizedTest
    @ValueSource(longs = {0, -1, -100, Long.MIN_VALUE})
    void requestWithInvalidCustomerIdsTest(Long id) throws Exception {

        // given
        RestoreRequest restoreRequest = new RestoreRequest(id, Set.of(1L, 2L));

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(restoreRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for customerId"));

    }

    @ParameterizedTest
    @NullSource
    void requestWithCustomerIdNullTest(Long id) throws Exception {

        // given
        RestoreRequest restoreRequest = new RestoreRequest(id, Set.of(1L, 2L));

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(restoreRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for customerId"));

    }

    @Test
    void requestWithRentalIdsEmptyTest() throws Exception {

        // given
        RestoreRequest restoreRequest = new RestoreRequest(1L, Collections.emptySet());

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(restoreRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for rentalIds"));

    }

    @Test
    void requestWithRentalIdsNullTest() throws Exception {

        // given
        RestoreRequest restoreRequest = new RestoreRequest(1L, null);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(restoreRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for rentalIds"));

    }

    @Test
    void requestWithCustomerIdNullAndRentalIdsNullTest() throws Exception {

        // given
        RestoreRequest restoreRequest = new RestoreRequest(null, null);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(restoreRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", Matchers.startsWith(("Validation failed for"))));

    }

}