package com.demo.challenge.rental.restore;

import com.demo.challenge.common.data.FilmBuilder;
import com.demo.challenge.customer.Customer;
import com.demo.challenge.customer.CustomerService;
import com.demo.challenge.film.Film;
import com.demo.challenge.rental.Rental;
import com.demo.challenge.rental.RentalCalculator;
import com.demo.challenge.rental.RentalService;
import com.demo.challenge.rental.restore.dto.RestoreRentalResponse;
import com.demo.challenge.rental.restore.dto.RestoreRequest;
import com.demo.challenge.rental.restore.dto.RestoreResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestoreServiceTest {

    @Mock
    private RentalService rentalService;

    @Mock
    private CustomerService customerService;

    @Spy
    private RentalCalculator rentalCalculator;

    @InjectMocks
    private RestoreService restoreService;

    @Test
    void restoreCustomerFilmsTest() {

        // given
        Customer customer = dummyCustomer();
        RestoreRequest restoreRequest = new RestoreRequest(customer.getId(), Set.of(1L, 2L, 3L));

        when(customerService.findCustomerById(anyLong())).thenReturn(customer);
        when(rentalService.findOpenRentalsForCustomer(anyLong(), anySet())).thenReturn(dummyRentals());

        // when
        RestoreResponse response = restoreService.restoreCustomerFilms(restoreRequest);

        // then
        List<RestoreRentalResponse> rentalResponses = response.getRestoreRentalResponses();

        assertEquals(customer.getId(), response.getCustomerId());
        assertEquals(3, rentalResponses.size());

        assertEquals(1, rentalResponses.get(0).getRentalId());
        assertEquals(1, rentalResponses.get(0).getFilmId());
        assertEquals(80, rentalResponses.get(0).getSurcharge());

        assertEquals(2, rentalResponses.get(1).getRentalId());
        assertEquals(2, rentalResponses.get(1).getFilmId());
        assertEquals(30, rentalResponses.get(1).getSurcharge());

        assertEquals(3, rentalResponses.get(2).getRentalId());
        assertEquals(3, rentalResponses.get(2).getFilmId());
        assertEquals(0, rentalResponses.get(2).getSurcharge());

        assertEquals(110, response.getTotalSurcharges());

    }

    @Test
    void restoreCustomerFilmsWithWrongRentalIdsTest() {

        // given
        Customer customer = dummyCustomer();
        RestoreRequest restoreRequest = new RestoreRequest(customer.getId(), Set.of(1L, 2L, 3L));

        when(customerService.findCustomerById(anyLong())).thenReturn(customer);
        when(rentalService.findOpenRentalsForCustomer(anyLong(), anySet())).thenReturn(Collections.emptyList());

        // when
        RestoreResponse response = restoreService.restoreCustomerFilms(restoreRequest);

        // then
        List<RestoreRentalResponse> rentalResponses = response.getRestoreRentalResponses();

        assertEquals(customer.getId(), response.getCustomerId());
        assertTrue(rentalResponses.isEmpty());
        assertEquals(0, response.getTotalSurcharges());

    }

    @Test
    void checkoutWithoutFilmsTest() {

        // given
        Customer customer = dummyCustomer();
        RestoreRequest restoreRequest = new RestoreRequest(customer.getId(), Collections.emptySet());

        when(customerService.findCustomerById(anyLong())).thenReturn(customer);
        when(rentalService.findOpenRentalsForCustomer(anyLong(), anySet())).thenReturn(Collections.emptyList());

        // when
        RestoreResponse response = restoreService.restoreCustomerFilms(restoreRequest);

        // then
        List<RestoreRentalResponse> rentalResponses = response.getRestoreRentalResponses();

        assertEquals(customer.getId(), response.getCustomerId());
        assertTrue(rentalResponses.isEmpty());
        assertEquals(0, response.getTotalSurcharges());

    }

    private List<Rental> dummyRentals() {

        FilmBuilder filmBuilder = new FilmBuilder();
        Film matrix11 = filmBuilder.getNewReleaseFilm(1, "Matrix 11");
        Film spiderman = filmBuilder.getRegularFilm(2, "Spider Man");
        Film outofAfrica = filmBuilder.getOldFilm(3, "Out of Africa");

        // late returns
        Rental rental1 = Rental.builder()
                .withId(1)
                .withFilm(matrix11)
                .withCustomer(dummyCustomer())
                .withRentalDate(LocalDateTime.now().minusDays(3))
                .withRentalDueDate(LocalDateTime.now().minusDays(2))
                .withCharge(0)
                .build();

        Rental rental2 = Rental.builder()
                .withId(2)
                .withFilm(spiderman)
                .withCustomer(dummyCustomer())
                .withRentalDate(LocalDateTime.now().minusDays(4))
                .withRentalDueDate(LocalDateTime.now().minusDays(1))
                .withCharge(0)
                .build();

        // on time return
        Rental rental3 = Rental.builder()
                .withId(3)
                .withFilm(outofAfrica)
                .withCustomer(dummyCustomer())
                .withRentalDate(LocalDateTime.now().minusDays(4))
                .withRentalDueDate(LocalDateTime.now().plusDays(3))
                .withCharge(0)
                .build();

        return List.of(rental1, rental2, rental3);

    }

    private Customer dummyCustomer() {
        return Customer.builder()
                .withId(1)
                .withName("John")
                .withLastname("Doe")
                .build();
    }

}