package com.demo.challenge.rental.checkout;

import com.demo.challenge.common.data.FilmBuilder;
import com.demo.challenge.common.exception.InvalidInputException;
import com.demo.challenge.customer.Customer;
import com.demo.challenge.customer.CustomerService;
import com.demo.challenge.film.Film;
import com.demo.challenge.film.FilmService;
import com.demo.challenge.rental.Rental;
import com.demo.challenge.rental.RentalCalculator;
import com.demo.challenge.rental.RentalService;
import com.demo.challenge.rental.checkout.dto.CheckoutRequest;
import com.demo.challenge.rental.checkout.dto.CheckoutResponse;
import com.demo.challenge.rental.checkout.dto.FilmRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckoutServiceTest {

    @Mock
    private FilmService filmService;

    @Mock
    private CustomerService customerService;

    @Mock
    private RentalService rentalService;

    @Spy
    private RentalCalculator rentCalculator;

    @InjectMocks
    private CheckoutService checkoutService;

    private final FilmBuilder filmBuilder = new FilmBuilder();

    @Test
    void checkoutTest() {

        // given
        Customer customer = dummyCustomer();

        Film matrix11 = filmBuilder.getNewReleaseFilm(1, "Matrix 11");
        Film spiderman = filmBuilder.getRegularFilm(2, "Spider Man");
        Film spiderman2 = filmBuilder.getRegularFilm(3,"Spider Man 2");
        Film outOfAfrica = filmBuilder.getOldFilm(4,"Out of Africa");

        List<FilmRequest> filmRequests = List.of(
                new FilmRequest(matrix11.getId(), 1),
                new FilmRequest(spiderman.getId(), 5),
                new FilmRequest(spiderman2.getId(), 2),
                new FilmRequest(outOfAfrica.getId(), 7)
        );

        when(customerService.findCustomerById(anyLong())).thenReturn(customer);
        when(filmService.findFilmsById(anySet())).thenReturn(List.of(matrix11, spiderman, spiderman2, outOfAfrica));
        when(rentalService.saveRental(any(Rental.class))).thenReturn(dummyRental());

        // when
        CheckoutRequest checkoutRequest = new CheckoutRequest(customer.getId(), filmRequests);
        CheckoutResponse response = checkoutService.checkout(checkoutRequest);

        // then
        assertEquals(customer.getId(), response.getCustomerId());

        assertEquals(4, response.getFilms().size());
        assertEquals(40, response.getFilms().get(0).getCharge());
        assertEquals(90, response.getFilms().get(1).getCharge());
        assertEquals(30, response.getFilms().get(2).getCharge());
        assertEquals(90, response.getFilms().get(3).getCharge());

        assertEquals(250,response.getTotalCost());
    }

    @Test
    void checkoutWithDuplicateFilmsInRequestTest() {

        // given
        Film matrix11 = filmBuilder.getNewReleaseFilm(1, "Matrix 11");

        List<FilmRequest> filmRequests = List.of(
                new FilmRequest(matrix11.getId(), 1),
                new FilmRequest(matrix11.getId(), 3)
        );

        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, filmRequests);

        // when
        Exception exception = assertThrows(InvalidInputException.class, () -> checkoutService.checkout(checkoutRequest));

        // then
        assertEquals("Cannot rent multiple copies of the same film", exception.getMessage());

    }

    @Test
    void checkoutWithInvalidFilmProvidedTest() {

        // given
        Customer customer = dummyCustomer();

        Film matrix11 = filmBuilder.getNewReleaseFilm(1, "Matrix 11");

        List<FilmRequest> filmRequests = List.of(
                new FilmRequest(matrix11.getId(), 3),
                new FilmRequest(3L, 5)
        );

        when(customerService.findCustomerById(anyLong())).thenReturn(customer);
        when(filmService.findFilmsById(anySet())).thenReturn(List.of(matrix11));
        when(rentalService.saveRental(any(Rental.class))).thenReturn(dummyRental());

        // when
        CheckoutRequest checkoutRequest = new CheckoutRequest(customer.getId(), filmRequests);
        CheckoutResponse response = checkoutService.checkout(checkoutRequest);

        // then
        assertEquals(customer.getId(), response.getCustomerId());
        assertEquals(1, response.getFilms().size());
        assertEquals(120, response.getFilms().get(0).getCharge());
        assertEquals(120,response.getTotalCost());

    }

    @Test
    void checkoutWithOpenRentalsTest() {

        // given
        Customer customer = dummyCustomer();
        Film matrix11 = filmBuilder.getNewReleaseFilm(1, "Matrix 11");

        List<FilmRequest> filmRequests = List.of(new FilmRequest(matrix11.getId(), 1));

        when(customerService.findCustomerById(anyLong())).thenReturn(customer);
        when(rentalService.findOpenRentalsForCustomer(anyLong())).thenReturn(List.of(dummyRental(matrix11)));

        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, filmRequests);

        // when
        Exception exception = assertThrows(InvalidInputException.class, () -> checkoutService.checkout(checkoutRequest));

        // then
        assertTrue(exception.getMessage().startsWith("Cannot rent a film if the previous one is not returned [Film id:"));

    }

    @Test
    void checkoutWithoutFilmsTest() {

        // given
        Customer customer = dummyCustomer();

        when(customerService.findCustomerById(anyLong())).thenReturn(customer);
        when(filmService.findFilmsById(anySet())).thenReturn(Collections.emptyList());

        // when
        CheckoutRequest checkoutRequest = new CheckoutRequest(customer.getId(), Collections.emptyList());
        CheckoutResponse response = checkoutService.checkout(checkoutRequest);

        // then
        assertEquals(customer.getId(), response.getCustomerId());
        assertTrue(response.getFilms().isEmpty());
        assertEquals(0, response.getTotalCost());

    }

    private Customer dummyCustomer() {
        return Customer.builder().withId(1).withName("John").withLastname("Doe").build();
    }

    private Rental dummyRental() {
        return Rental.builder().withId(1).build();
    }

    private Rental dummyRental(Film film) {
        return dummyRental().toBuilder().withFilm(film).build();
    }

}