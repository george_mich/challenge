package com.demo.challenge.rental.checkout;

import com.demo.challenge.common.exception.ExceptionHandlers;
import com.demo.challenge.common.exception.InvalidInputException;
import com.demo.challenge.rental.checkout.dto.CheckoutRequest;
import com.demo.challenge.rental.checkout.dto.CheckoutResponse;
import com.demo.challenge.rental.checkout.dto.FilmRequest;
import com.demo.challenge.rental.checkout.dto.FilmResponse;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static com.demo.challenge.common.TestUtil.toJson;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class CheckoutControllerTest {

    private static final String URL = "/api/rental/checkout";

    @Mock
    private CheckoutService checkoutService;

    @InjectMocks
    private CheckoutController checkoutController;

    private MockMvc mockMvc;

    @BeforeEach
    void beforeEach() {
        mockMvc = MockMvcBuilders.standaloneSetup(checkoutController)
                .setControllerAdvice(new ExceptionHandlers())
                .build();
    }

    @Test
    void validRequestTest() throws Exception {

        // given
        List<FilmRequest> filmRequests = List.of( new FilmRequest(1L,1));
        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, filmRequests);

        List<FilmResponse> filmResponses = List.of(new FilmResponse(1, 30, 1, 30.0));
        CheckoutResponse checkoutResponse = new CheckoutResponse(1, filmResponses, 30.0);

        when(checkoutService.checkout(any())).thenReturn(checkoutResponse);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.customerId").value("1"))
                .andExpect(jsonPath("$.films[0].filmId").value("1"))
                .andExpect(jsonPath("$.films[0].dueDays").value("30"))
                .andExpect(jsonPath("$.films[0].rentId").value("1"))
                .andExpect(jsonPath("$.films[0].charge").value("30.0"));

    }

    @Test
    void invalidRequestTest() throws Exception {

        // given
        String invalidRequest = "invalid request";

        // when
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(invalidRequest))
                // then
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
    }

    @Test
    void invalidInputExceptionHandlerTest() throws Exception {

        // given
        List<FilmRequest> filmRequests = List.of( new FilmRequest(1L,1));
        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, filmRequests);

        String errorMessage = "This is an error message";

        when(checkoutService.checkout(any())).thenThrow(new InvalidInputException(errorMessage));

        // when
        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(errorMessage));
    }

    @ParameterizedTest
    @ValueSource(longs = {0, -1, -100, Long.MIN_VALUE})
    void requestWithInvalidCustomerIdsTest(long customerId) throws Exception {

        // given
        List<FilmRequest> filmRequests = List.of( new FilmRequest(1L,1));
        CheckoutRequest checkoutRequest = new CheckoutRequest(customerId, filmRequests);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for customerId"));
    }

    @ParameterizedTest
    @NullSource
    void requestWithNullCustomerIdTest(Long customerId) throws Exception {

        // given
        List<FilmRequest> filmRequests = List.of( new FilmRequest(1L,1));
        CheckoutRequest checkoutRequest = new CheckoutRequest(customerId, filmRequests);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for customerId"));
    }

    @Test
    void requestWithNullFilmsTest() throws Exception {

        // given
        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, null);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for filmRequests"));

    }

    @Test
    void requestWithEmptyFilmsTest() throws Exception {

        // given
        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, Collections.emptyList());

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for filmRequests"));

    }

    @ParameterizedTest
    @ValueSource(longs = {0, -1, -100, Long.MIN_VALUE})
    void requestWithInvalidFilmRequestsFilmIdTest(Long filmId) throws Exception {

        // given
        List<FilmRequest> filmRequests = List.of(new FilmRequest(filmId, 5));
        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, filmRequests);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for filmRequests[0].filmId"));

    }

    @ParameterizedTest
    @ValueSource(ints = {0, -1, -100, Integer.MIN_VALUE})
    void requestWithInvalidFilmRequestsDaysTest(Integer days) throws Exception {

        // given
        List<FilmRequest> filmRequests = List.of(new FilmRequest(5L, days));
        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, filmRequests);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Validation failed for filmRequests[0].days"));

    }

    @ParameterizedTest
    @CsvSource({"0,0", "-1,0", "0,-1", "-1,-1", "-14,-67"})
    void requestWithInvalidFilmRequestsTest(Long filmId, Integer days) throws Exception {

        // given
        List<FilmRequest> filmRequests = List.of(new FilmRequest(filmId, days));
        CheckoutRequest checkoutRequest = new CheckoutRequest(1L, filmRequests);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(checkoutRequest)))
                // then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", Matchers.startsWith(("Validation failed for filmRequests[0]."))));
    }

}