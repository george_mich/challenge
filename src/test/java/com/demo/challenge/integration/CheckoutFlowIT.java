package com.demo.challenge.integration;

import com.demo.challenge.common.exception.ErrorResponse;
import com.demo.challenge.customer.Customer;
import com.demo.challenge.customer.CustomerRepository;
import com.demo.challenge.film.Film;
import com.demo.challenge.film.FilmRepository;
import com.demo.challenge.rental.Rental;
import com.demo.challenge.rental.RentalRepository;
import com.demo.challenge.rental.checkout.dto.CheckoutRequest;
import com.demo.challenge.rental.checkout.dto.CheckoutResponse;
import com.demo.challenge.rental.checkout.dto.FilmRequest;
import com.demo.challenge.rental.checkout.dto.FilmResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CheckoutFlowIT {

    private static final String CHECKOUT_URI = "/api/rental/checkout";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RentalRepository rentalRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    void beforeEach() {
        rentalRepository.deleteAll();
    }

    @Test
    @Transactional
    void checkoutFlowTest() {

        // given
        Optional<Customer> optionalCustomer = customerRepository.findById(1L);
        assertTrue(optionalCustomer.isPresent());

        Customer customer = optionalCustomer.get();

        CheckoutRequest checkoutRequest = prepareCheckoutRequest(customer.getId());

        // when
        ResponseEntity<CheckoutResponse> response = doPostRequest(checkoutRequest, CheckoutResponse.class);

        // then
        assertEquals(200, response.getStatusCodeValue());

        HttpHeaders httpHeaders = response.getHeaders();
        CheckoutResponse checkoutResponse = response.getBody();

        assertNotNull(httpHeaders);
        assertNotNull(checkoutResponse);

        List<String> contentType = httpHeaders.get("Content-Type");
        assertNotNull(contentType);

        assertTrue(contentType.contains("application/json"));

        List<FilmResponse> filmResponses = checkoutResponse.getFilms();

        log.info("Comparing response against database");

        filmResponses.forEach(filmResponse -> {

            Optional<Rental> optionalRental = rentalRepository.findById(filmResponse.getRentId());
            assertTrue(optionalRental.isPresent());

            Rental rental = optionalRental.get();

            assertEquals(checkoutRequest.getCustomerId(), rental.getCustomer().getId());
            assertTrue(customer.getRentals().contains(rental));

            assertEquals(filmResponse.getRentId(), rental.getId());
            assertEquals(filmResponse.getFilmId(), rental.getFilm().getId());
            assertEquals(filmResponse.getCharge(), rental.getCharge());

            assertEquals(filmResponse.getDueDays(), daysBetween(rental.getRentalDate(), rental.getRentalDueDate()));

            assertNull(rental.getReturnDate());
            assertEquals(0, rental.getSurcharge());
            log.info("Rental: {}", rental);

        });

        assertEquals(250, checkoutResponse.getTotalCost());
        assertCustomer(customer, checkoutResponse);
        assertFilmResponses(filmResponses);

    }

    @Test
    void invalidCustomerTest() {

        // given
        CheckoutRequest checkoutRequest = prepareCheckoutRequest(100);

        // when
        ResponseEntity<ErrorResponse> response = doPostRequest(checkoutRequest, ErrorResponse.class);

        // then
        assertEquals(404, response.getStatusCodeValue());
        HttpHeaders httpHeaders = response.getHeaders();
        ErrorResponse errorResponse = response.getBody();

        assertNotNull(httpHeaders);
        assertNotNull(errorResponse);

        List<String> contentType = httpHeaders.get("Content-Type");
        assertNotNull(contentType);

        assertTrue(contentType.contains("application/json"));
        assertEquals("Customer not found with id 100", errorResponse.getMessage());

    }

    private void assertCustomer(Customer customer, CheckoutResponse checkoutResponse) {
        assertEquals(customer.getId(), checkoutResponse.getCustomerId());
        assertEquals("John", customer.getName());
        assertEquals("Doe", customer.getLastname());
    }

    private void assertFilmResponses(List<FilmResponse> filmResponses) {

        assertEquals(1, filmResponses.get(0).getFilmId());
        assertEquals(1, filmResponses.get(0).getRentId());
        assertEquals(1, filmResponses.get(0).getDueDays());
        assertEquals(40, filmResponses.get(0).getCharge());

        assertEquals(2, filmResponses.get(1).getFilmId());
        assertEquals(2, filmResponses.get(1).getRentId());
        assertEquals(5, filmResponses.get(1).getDueDays());
        assertEquals(90, filmResponses.get(1).getCharge());

        assertEquals(3, filmResponses.get(2).getFilmId());
        assertEquals(3, filmResponses.get(2).getRentId());
        assertEquals(3, filmResponses.get(2).getDueDays());
        assertEquals(30, filmResponses.get(2).getCharge());

        assertEquals(4, filmResponses.get(3).getFilmId());
        assertEquals(4, filmResponses.get(3).getRentId());
        assertEquals(7, filmResponses.get(3).getDueDays());
        assertEquals(90, filmResponses.get(3).getCharge());

    }

    private <T> ResponseEntity<T> doPostRequest(CheckoutRequest checkoutRequest, Class<T> responseType) {
        return restTemplate.postForEntity(CHECKOUT_URI, checkoutRequest, responseType);
    }

    private CheckoutRequest prepareCheckoutRequest(long customerId) {

        Map<String, Long> catalogue = filmRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(Film::getName, Film::getId));

        List<FilmRequest> filmRequests = List.of(
                new FilmRequest(catalogue.get("Matrix 11"), 1),
                new FilmRequest(catalogue.get("Spider Man"), 5),
                new FilmRequest(catalogue.get("Spider Man 2"), 2),
                new FilmRequest(catalogue.get("Out of Africa"), 7)
        );

        return new CheckoutRequest(customerId, filmRequests);

    }

    private long daysBetween(LocalDateTime from, LocalDateTime to) {
        return ChronoUnit.DAYS.between(from, to);
    }

}
