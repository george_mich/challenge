package com.demo.challenge.integration;

import com.demo.challenge.customer.Customer;
import com.demo.challenge.customer.CustomerRepository;
import com.demo.challenge.film.Film;
import com.demo.challenge.film.FilmRepository;
import com.demo.challenge.rental.Rental;
import com.demo.challenge.rental.RentalRepository;
import com.demo.challenge.rental.restore.dto.RestoreRentalResponse;
import com.demo.challenge.rental.restore.dto.RestoreRequest;
import com.demo.challenge.rental.restore.dto.RestoreResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RestoreFlowIT {

    private static final String RESTORE_URI = "/api/rental/restore";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RentalRepository rentalRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    void beforeEach() {
        rentalRepository.deleteAll();
    }

    @Test
    void restoreFlowTest() {

        // given
        Optional<Customer> optionalCustomer = customerRepository.findById(1L);
        assertTrue(optionalCustomer.isPresent());
        Customer customer = optionalCustomer.get();

        List<Rental> rentals = createDummyRentals(customer);
        Set<Long> rentalIds = rentals.stream().map(Rental::getId).collect(Collectors.toSet());

        // when
        RestoreRequest restoreRequest = new RestoreRequest(customer.getId(), rentalIds);
        ResponseEntity<RestoreResponse> response = restTemplate.postForEntity(RESTORE_URI, restoreRequest, RestoreResponse.class);

        // then
        assertEquals(200, response.getStatusCodeValue());

        HttpHeaders httpHeaders = response.getHeaders();
        RestoreResponse body = response.getBody();

        assertNotNull(httpHeaders);
        assertNotNull(body);

        List<String> contentType = httpHeaders.get("Content-Type");
        assertNotNull(contentType);

        assertTrue(contentType.contains("application/json"));


        RestoreResponse restoreResponse = response.getBody();

        List<RestoreRentalResponse> rentalResponses = restoreResponse.getRestoreRentalResponses();
        assertEquals(rentalIds.size(), rentalResponses.size());

        rentalResponses.forEach(rentalResponse -> {

            Optional<Rental> optionalRental = rentalRepository.findById(rentalResponse.getRentalId());
            assertTrue(optionalRental.isPresent());

            Rental rental = optionalRental.get();

            Assertions.assertEquals(restoreResponse.getCustomerId(), rental.getCustomer().getId());
            Assertions.assertEquals(rentalResponse.getFilmId(), rental.getFilm().getId());
            assertEquals(rentalResponse.getSurcharge(), rental.getSurcharge());
            assertNotNull(rental.getReturnDate());

        });

        assertEquals(customer.getId(), restoreResponse.getCustomerId());
        assertEquals(110, restoreResponse.getTotalSurcharges());

        assertEquals(1, rentalResponses.get(0).getRentalId());
        assertEquals(1, rentalResponses.get(0).getFilmId());
        assertEquals(80, rentalResponses.get(0).getSurcharge());

        assertEquals(2, rentalResponses.get(1).getRentalId());
        assertEquals(2, rentalResponses.get(1).getFilmId());
        assertEquals(30, rentalResponses.get(1).getSurcharge());

    }

    private List<Rental> createDummyRentals(Customer customer) {

        List<Film> films = filmRepository
                .findAll()
                .stream()
                .filter(film -> film.getName().equals("Matrix 11") || film.getName().equals("Spider Man"))
                .collect(Collectors.toList());

        Rental rental1 = Rental.builder()
                .withFilm(films.get(0))
                .withCustomer(customer)
                .withRentalDate(LocalDateTime.now().minusDays(3))
                .withRentalDueDate(LocalDateTime.now().minusDays(2))
                .withCharge(40)
                .build();

        Rental rental2 = Rental.builder()
                .withFilm(films.get(1))
                .withCustomer(customer)
                .withRentalDate(LocalDateTime.now().minusDays(4))
                .withRentalDueDate(LocalDateTime.now().minusDays(1))
                .withCharge(30)
                .build();

        return rentalRepository.saveAll(List.of(rental1, rental2));

    }

}
