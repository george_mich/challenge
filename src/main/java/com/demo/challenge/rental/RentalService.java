package com.demo.challenge.rental;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class RentalService {

    private final RentalRepository rentalRepository;

    public Rental saveRental(Rental rental) {
        return rentalRepository.save(rental);
    }

    public List<Rental> findOpenRentalsForCustomer(long customerId) {
        return rentalRepository.findAllByCustomerIdAndReturnDateIsNull(customerId);
    }

    public List<Rental> findOpenRentalsForCustomer(long customerId, Set<Long> ids) {
        return rentalRepository.findAllByCustomerIdAndReturnDateIsNullAndIdIn(customerId, ids);
    }

}
