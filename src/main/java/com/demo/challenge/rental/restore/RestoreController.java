package com.demo.challenge.rental.restore;

import com.demo.challenge.rental.restore.dto.RestoreRequest;
import com.demo.challenge.rental.restore.dto.RestoreResponse;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/rental/restore")
@RequiredArgsConstructor
@Api(tags="Rental")
public class RestoreController {

    private final RestoreService restoreService;

    @PostMapping
    public ResponseEntity<RestoreResponse> restoreFilms(@RequestBody @Valid RestoreRequest restoreRequest) {
        RestoreResponse restoreResponse = restoreService.restoreCustomerFilms(restoreRequest);
        return ResponseEntity.ok(restoreResponse);
    }

}
