package com.demo.challenge.rental.restore.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RestoreRentalResponse {

    private final long rentalId;
    private final long filmId;
    private final double surcharge;

}
