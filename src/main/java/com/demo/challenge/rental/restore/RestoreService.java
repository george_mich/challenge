package com.demo.challenge.rental.restore;

import com.demo.challenge.customer.Customer;
import com.demo.challenge.customer.CustomerService;
import com.demo.challenge.rental.Rental;
import com.demo.challenge.rental.RentalCalculator;
import com.demo.challenge.rental.RentalService;
import com.demo.challenge.rental.restore.dto.RestoreRentalResponse;
import com.demo.challenge.rental.restore.dto.RestoreRequest;
import com.demo.challenge.rental.restore.dto.RestoreResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class RestoreService {

    private final RentalService rentalService;
    private final CustomerService customerService;
    private final RentalCalculator rentalCalculator;

    public RestoreResponse restoreCustomerFilms(RestoreRequest restoreRequest) {

        List<RestoreRentalResponse> restoreRentalResponses = new ArrayList<>();
        AtomicReference<Double> totalSurcharges = new AtomicReference<>( 0.0);

        long customerId = restoreRequest.getCustomerId();
        Customer customer = customerService.findCustomerById(customerId);

        List<Rental> rentals = rentalService.findOpenRentalsForCustomer(restoreRequest.getCustomerId(), restoreRequest.getRentalIds());

        rentals.forEach(rental -> {

            LocalDateTime returnedDate = LocalDateTime.now();
            long offsetDueDate = Math.max(0, Duration.between(rental.getRentalDueDate(), returnedDate).toDays());
            double surcharge = rentalCalculator.calculateSurcharge(rental.getFilm(), offsetDueDate);

            totalSurcharges.updateAndGet(v -> v + surcharge);

            Rental updatedRental = updateRental(rental, returnedDate, surcharge);

            rentalService.saveRental(updatedRental);
            restoreRentalResponses.add(new RestoreRentalResponse(rental.getId(), rental.getFilm().getId(), surcharge));
        });
        return new RestoreResponse(customer.getId(), restoreRentalResponses, totalSurcharges.get());
    }

    private Rental updateRental(Rental rental, LocalDateTime returnDate, double surcharge) {
        return rental.toBuilder()
                .withReturnDate(returnDate)
                .withSurcharge(surcharge)
                .build();
    }

}
