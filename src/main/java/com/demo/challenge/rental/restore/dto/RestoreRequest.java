package com.demo.challenge.rental.restore.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RestoreRequest {

    @NotNull
    @Min(1)
    private Long customerId;

    @NotNull
    @NotEmpty
    @JsonProperty("rentals")
    private Set<Long> rentalIds;

}
