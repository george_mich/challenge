package com.demo.challenge.rental.restore.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class RestoreResponse {

    private final long customerId;

    @JsonProperty("rentals")
    private final List<RestoreRentalResponse> restoreRentalResponses;

    private final double totalSurcharges;

}
