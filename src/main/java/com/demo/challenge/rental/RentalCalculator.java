package com.demo.challenge.rental;

import com.demo.challenge.film.Film;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class RentalCalculator {

    public double calculateCostForFilm(Film film, int rentalDays) {

        if(rentalDays <= 0) {
            return 0;
        }

        double filmPrice = getFilmPrice(film);
        double extraRentalDays = Math.max(0, rentalDays - film.getFilmType().getDefaultRentalDays());

        return filmPrice + (extraRentalDays * filmPrice);
    }

    public double calculateSurcharge(Film film, long offsetDueDate) {

        if(offsetDueDate <= 0) {
            return 0;
        }

        return getFilmPrice(film) * offsetDueDate;
    }

    private double getFilmPrice(Film film) {
        return film.getFilmType().getPriceType().getPrice();
    }

}
