package com.demo.challenge.rental;

import com.demo.challenge.customer.Customer;
import com.demo.challenge.film.Film;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with", toBuilder = true)
@Getter
public class Rental {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Film film;

    private LocalDateTime rentalDate;

    private LocalDateTime rentalDueDate;

    private LocalDateTime returnDate;

    private double charge;

    private double surcharge;

}
