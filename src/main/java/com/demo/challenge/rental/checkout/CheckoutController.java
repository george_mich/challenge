package com.demo.challenge.rental.checkout;

import com.demo.challenge.rental.checkout.dto.CheckoutRequest;
import com.demo.challenge.rental.checkout.dto.CheckoutResponse;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/rental/checkout")
@RequiredArgsConstructor
@Api(tags="Rental")
public class CheckoutController {

    private final CheckoutService checkoutService;

    @PostMapping
    public ResponseEntity<CheckoutResponse> checkout(@RequestBody @Valid CheckoutRequest checkoutRequest) {
        CheckoutResponse response  = checkoutService.checkout(checkoutRequest);
        return ResponseEntity.ok(response);
    }

}
