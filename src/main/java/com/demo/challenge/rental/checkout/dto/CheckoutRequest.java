package com.demo.challenge.rental.checkout.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CheckoutRequest {

    @NotNull
    @Min(1)
    private Long customerId;

    @NotEmpty
    @NotNull
    @Valid
    private List<FilmRequest> filmRequests;

}
