package com.demo.challenge.rental.checkout.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FilmResponse {

    private long filmId;
    private long dueDays;
    private long rentId;
    private double charge;

}
