package com.demo.challenge.rental.checkout;

import com.demo.challenge.common.exception.InvalidInputException;
import com.demo.challenge.customer.Customer;
import com.demo.challenge.customer.CustomerService;
import com.demo.challenge.film.Film;
import com.demo.challenge.film.FilmService;
import com.demo.challenge.rental.RentalCalculator;
import com.demo.challenge.rental.Rental;
import com.demo.challenge.rental.RentalService;
import com.demo.challenge.rental.checkout.dto.CheckoutRequest;
import com.demo.challenge.rental.checkout.dto.CheckoutResponse;
import com.demo.challenge.rental.checkout.dto.FilmRequest;
import com.demo.challenge.rental.checkout.dto.FilmResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CheckoutService {

    private final FilmService filmService;
    private final CustomerService customerService;
    private final RentalService rentalService;
    private final RentalCalculator rentCalculator;

    CheckoutResponse checkout(CheckoutRequest checkoutRequest) {

        // data preparation and validations
        Customer customer = customerService.findCustomerById(checkoutRequest.getCustomerId());
        Map<Long, Integer> filmIdsToDaysMap = mapFilmIdsToDays(checkoutRequest.getFilmRequests());
        verifyOpenRentalsForCustomer(customer.getId(), checkoutRequest.getFilmRequests());

        List<FilmResponse> filmResponses = new ArrayList<>();
        AtomicReference<Double> totalCost = new AtomicReference<>( 0.0);

        // for each film add a rent entry
        filmService.findFilmsById(filmIdsToDaysMap.keySet())
                .forEach(film -> {

                    int requestedDays = filmIdsToDaysMap.get(film.getId());
                    double charge = rentCalculator.calculateCostForFilm(film, requestedDays);

                    Rental rental = buildRental(customer, film, requestedDays, charge);
                    long rentalId = rentalService.saveRental(rental).getId();

                    totalCost.updateAndGet(v -> v + charge);
                    filmResponses.add(new FilmResponse(rentalId, ChronoUnit.DAYS.between(rental.getRentalDate(),
                            rental.getRentalDueDate()), rental.getId(), rental.getCharge()));

        });

        return new CheckoutResponse(customer.getId(), filmResponses, totalCost.get());
    }

    private Map<Long, Integer> mapFilmIdsToDays(List<FilmRequest> filmRequests) {

        long distinctCount = filmRequests.stream().mapToLong(FilmRequest::getFilmId).distinct().count();

        if(filmRequests.size() > distinctCount) {
            throw new InvalidInputException("Cannot rent multiple copies of the same film");
        }

        return filmRequests.stream().collect(Collectors.toMap(FilmRequest::getFilmId, FilmRequest::getDays));
    }

    private void verifyOpenRentalsForCustomer(long customerId, List<FilmRequest> filmRequests) {

        List<Long> openRentalsForCustomer = rentalService.findOpenRentalsForCustomer(customerId)
                .stream().map(rental -> rental.getFilm().getId())
                .collect(Collectors.toList());

        filmRequests
                .stream()
                .map(FilmRequest::getFilmId)
                .filter(openRentalsForCustomer::contains)
                .findAny()
                .ifPresent(filmId -> {
                    throw new InvalidInputException("Cannot rent a film if the previous one is not returned [Film id: " + filmId + "]");
                });
    }

    private Rental buildRental(Customer customer, Film film, int requestedDays, double charge) {

        int defaultDays = film.getFilmType().getDefaultRentalDays();
        LocalDateTime now = LocalDateTime.now();

        return Rental.builder()
                .withCustomer(customer)
                .withFilm(film)
                .withRentalDate(now)
                .withRentalDueDate(now.plusDays(Math.max(defaultDays, requestedDays)))
                .withCharge(charge)
                .build();
    }

}
