package com.demo.challenge.rental.checkout.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CheckoutResponse {

    private long customerId;
    private List<FilmResponse> films;
    private double totalCost;

}
