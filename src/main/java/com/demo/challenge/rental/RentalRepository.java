package com.demo.challenge.rental;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RentalRepository extends JpaRepository<Rental, Long> {

    List<Rental> findAllByCustomerIdAndReturnDateIsNull(long customerId);

    List<Rental> findAllByCustomerIdAndReturnDateIsNullAndIdIn(long customerId, Set<Long> id);

}
