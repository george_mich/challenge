package com.demo.challenge.film;

import com.demo.challenge.filmtype.FilmType;
import com.demo.challenge.rental.Rental;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(setterPrefix = "with")
@Getter
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @OneToOne
    @NotNull
    private FilmType filmType;

    @OneToMany(mappedBy = "film")
    private Set<Rental> rentals;

}
