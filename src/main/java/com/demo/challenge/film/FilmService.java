package com.demo.challenge.film;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class FilmService {

    private final FilmRepository filmRepository;

    public void saveFilms(List<Film> films) {
        filmRepository.saveAll(films);
    }

    public List<Film> findFilmsById(Set<Long> filmIds) {
        return filmRepository.findAllById(filmIds);
    }

}
