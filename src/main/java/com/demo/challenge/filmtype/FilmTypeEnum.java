package com.demo.challenge.filmtype;

public enum FilmTypeEnum {

    NEW_RELEASE("New Release"),
    REGULAR("Regular"),
    OLD("Old");

    private final String value;

    FilmTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
