package com.demo.challenge.filmtype;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FilmTypeRepository extends JpaRepository<FilmType, Long> {

    Optional<FilmType> findByType(String filmType);

}
