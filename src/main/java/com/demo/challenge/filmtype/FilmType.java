package com.demo.challenge.filmtype;

import com.demo.challenge.pricetype.PriceType;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(setterPrefix = "with")
@Getter
public class FilmType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String type;

    @OneToOne
    @NotNull
    private PriceType priceType;

    @NotNull
    private int defaultRentalDays;

}
