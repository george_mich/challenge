package com.demo.challenge.filmtype;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FilmTypeService {

    private final FilmTypeRepository filmTypeRepository;

    public void saveFilmTypes(List<FilmType> filmTypes) {
        filmTypeRepository.saveAll(filmTypes);
    }

    public Optional<FilmType> findByType(String filmType) {
        return filmTypeRepository.findByType(filmType);
    }

}
