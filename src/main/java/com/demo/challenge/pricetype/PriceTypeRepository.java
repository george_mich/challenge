package com.demo.challenge.pricetype;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PriceTypeRepository extends JpaRepository<PriceType, Long> {

    Optional<PriceType> findByType(String type);

}
