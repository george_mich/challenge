package com.demo.challenge.pricetype;

public enum PriceTypeEnum {

    BASIC("Basic"),
    PREMIUM("Premium");

    private final String value;

    PriceTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
