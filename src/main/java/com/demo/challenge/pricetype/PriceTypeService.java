package com.demo.challenge.pricetype;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PriceTypeService {

    private final PriceTypeRepository priceRepository;

    public void savePrices(List<PriceType> prices) {
        priceRepository.saveAll(prices);
    }

    public Optional<PriceType> findPriceByName(String type) {
        return priceRepository.findByType(type);
    }

}
