package com.demo.challenge.common.data.importer;

import com.demo.challenge.film.Film;
import com.demo.challenge.film.FilmService;
import com.demo.challenge.filmtype.FilmType;
import com.demo.challenge.filmtype.FilmTypeEnum;
import com.demo.challenge.filmtype.FilmTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FilmImporter extends ImportTemplate<Film> {

    private final FilmTypeService filmTypeService;
    private final FilmService filmService;

    @Override
    protected List<Film> createData() {

        Optional<FilmType> optionalNewRelease = filmTypeService.findByType(FilmTypeEnum.NEW_RELEASE.getValue());
        Optional<FilmType> optionalRegular = filmTypeService.findByType(FilmTypeEnum.REGULAR.getValue());
        Optional<FilmType> optionalOld = filmTypeService.findByType(FilmTypeEnum.OLD.getValue());

        Film matrix11 = Film.builder()
                .withName("Matrix 11")
                .withFilmType(optionalNewRelease.orElse(null))
                .build();

        Film spiderman = Film.builder()
                .withName("Spider Man")
                .withFilmType(optionalRegular.orElse(null))
                .build();

        Film spiderman2 = Film.builder()
                .withName("Spider Man 2")
                .withFilmType(optionalRegular.orElse(null))
                .build();

        Film outOfAfrica = Film.builder()
                .withName("Out of Africa")
                .withFilmType(optionalOld.orElse(null))
                .build();

        return List.of(matrix11, spiderman, spiderman2, outOfAfrica);
    }

    @Override
    protected void store(List<Film> data) {
        filmService.saveFilms(data);
    }

}
