package com.demo.challenge.common.data.importer;

import java.util.List;

public abstract class ImportTemplate<T> {

    protected abstract List<T> createData();
    protected abstract void store(List<T> data);

    public void importData() {
        List<T> data = createData();
        store(data);
    }

}
