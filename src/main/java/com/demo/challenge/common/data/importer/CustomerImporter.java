package com.demo.challenge.common.data.importer;

import com.demo.challenge.customer.Customer;
import com.demo.challenge.customer.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerImporter extends ImportTemplate<Customer> {

    private final CustomerService customerService;

    @Override
    protected List<Customer> createData() {

        Customer customer1 = Customer.builder()
                .withName("John")
                .withLastname("Doe")
                .build();

        Customer customer2 = Customer.builder()
                .withName("Steve")
                .withLastname("Bo")
                .build();

        return List.of(customer1, customer2);
    }

    @Override
    protected void store(List<Customer> data) {
        customerService.saveCustomers(data);
    }

}
