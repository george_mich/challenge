package com.demo.challenge.common.data.importer;

import com.demo.challenge.filmtype.FilmType;
import com.demo.challenge.filmtype.FilmTypeEnum;
import com.demo.challenge.filmtype.FilmTypeService;
import com.demo.challenge.pricetype.PriceType;
import com.demo.challenge.pricetype.PriceTypeEnum;
import com.demo.challenge.pricetype.PriceTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FilmTypeImporter extends ImportTemplate<FilmType> {

    private final PriceTypeService priceTypeService;
    private final FilmTypeService filmTypeService;

    @Override
    protected List<FilmType> createData() {

        Optional<PriceType> optionalPremium = priceTypeService.findPriceByName(PriceTypeEnum.PREMIUM.getValue());
        Optional<PriceType> optionalBasic = priceTypeService.findPriceByName(PriceTypeEnum.BASIC.getValue());

        FilmType newRelease = FilmType.builder()
                .withType(FilmTypeEnum.NEW_RELEASE.getValue())
                .withDefaultRentalDays(1)
                .withPriceType(optionalPremium.orElse(null))
                .build();

        FilmType regular = FilmType.builder()
                .withType(FilmTypeEnum.REGULAR.getValue())
                .withDefaultRentalDays(3)
                .withPriceType(optionalBasic.orElse(null))
                .build();

        FilmType old = FilmType.builder()
                .withType(FilmTypeEnum.OLD.getValue())
                .withDefaultRentalDays(5)
                .withPriceType(optionalBasic.orElse(null))
                .build();

        return List.of(newRelease, regular, old);
    }

    @Override
    protected void store(List<FilmType> data) {
        filmTypeService.saveFilmTypes(data);
    }
}
