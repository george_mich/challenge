package com.demo.challenge.common.data.importer;

import com.demo.challenge.pricetype.PriceType;
import com.demo.challenge.pricetype.PriceTypeEnum;
import com.demo.challenge.pricetype.PriceTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PriceTypeImporter extends ImportTemplate<PriceType> {

    private final PriceTypeService priceTypeService;

    @Override
    protected List<PriceType> createData() {
        PriceType premium = PriceType.builder()
                .withType(PriceTypeEnum.PREMIUM.getValue())
                .withPrice(40.00)
                .build();

        PriceType basic = PriceType.builder()
                .withType(PriceTypeEnum.BASIC.getValue())
                .withPrice(30.00)
                .build();

        return List.of(premium, basic);
    }

    @Override
    protected void store(List<PriceType> data) {
        priceTypeService.savePrices(data);
    }

}
