package com.demo.challenge.common.data;

import com.demo.challenge.common.data.importer.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DataInitializer implements ApplicationRunner {

    private final PriceTypeImporter priceTypeImporter;
    private final FilmTypeImporter filmTypeImporter;
    private final FilmImporter filmImporter;
    private final CustomerImporter customerImporter;

    @Override
    public void run(ApplicationArguments args) {
        registeredDataImporters().forEach(ImportTemplate::importData);
    }

    private List<ImportTemplate<?>> registeredDataImporters() {

        return List.of(
                priceTypeImporter,
                filmTypeImporter,
                filmImporter,
                customerImporter
        );

    }

}
