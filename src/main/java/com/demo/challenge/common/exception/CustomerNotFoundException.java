package com.demo.challenge.common.exception;

public class CustomerNotFoundException extends RuntimeException {

    public CustomerNotFoundException(long id) {
        super("Customer not found with id " + id);
    }

}
