package com.demo.challenge.common.exception;

public class InvalidInputException extends RuntimeException{

    public InvalidInputException(String message) {
        super(message);
    }

}
