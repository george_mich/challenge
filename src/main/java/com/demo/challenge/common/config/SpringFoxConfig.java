package com.demo.challenge.common.config;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.demo.challenge"))
                .paths(PathSelectors.any())
                .build()
                .tags(new Tag("Rental", "Rental APIs"))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {

        Contact contact = new Contact(
                "George Michael",
                "https://www.linkedin.com/in/george-michael-17440b11b",
                null);

        return new ApiInfoBuilder()
                .title("Rental API Documentation")
                .description("Rental Management which enables user to rent and restore films while calculating the costs")
                .version("0.0.1")
                .contact(contact)
                .build();
    }

}
